<?php

namespace App\Repository;

use App\Entity\News;
use App\Entity\NewsCategory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method News|null find($id, $lockMode = null, $lockVersion = null)
 * @method News|null findOneBy(array $criteria, array $orderBy = null)
 * @method News[]    findAll()
 * @method News[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NewsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, News::class);
    }

    public function findNewsActiveQueryByCategory(NewsCategory $category = null)
    {
        $queryBuilder = $this->createQueryBuilder('n')
            ->andWhere('n.active = :active')
            ->setParameter('active', true)
            ->orderBy('n.createdAt', 'DESC');

        if (null !== $category) {
            $queryBuilder->andWhere('n.category = :category')
                ->setParameter('category', $category->getId());
        }

        return $queryBuilder->getQuery();
    }

    public function findNewsByTitle(?string $title = null)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.active = :active')
            ->andWhere('n.title LIKE :title')
            ->setParameter('active', true)
            ->setParameter('title', '%'.$title.'%')
            ->orderBy('n.createdAt', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findNewsByTag(string $tag)
    {
        return $this->createQueryBuilder('n')
            ->where('n.tags LIKE :tags')
            ->setParameter('tags', '%"'.$tag.'"%')
            ->getQuery()
            ->getResult();
    }

}
