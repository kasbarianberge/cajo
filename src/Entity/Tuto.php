<?php

namespace App\Entity;

use App\Repository\TutoRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Entity\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;


/**
 * @ORM\Entity(repositoryClass=TutoRepository::class)
 * @Vich\Uploadable
 */
class Tuto
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private bool $active = true; 
    
    /**
     * @ORM\ManyToOne(targetEntity=LaBaseCategory::class)
     * @ORM\JoinColumn(nullable=true)
     */
    private LaBaseCategory $category;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=255)
     */
    private ?string $title;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="text")
     */
    private ?string $content;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=true)
     */
    private ?User $author;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Keywords", inversedBy="mentionsTutos", cascade={"persist"})
     * @ORM\JoinTable(name="tutos_keywords")
     * @ORM\OrderBy({"keyword" = "ASC"})
     */
    private $listekeywords;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Lexique", inversedBy="mentionsTutos", cascade={"persist"})
     * @ORM\JoinTable(name="tutos_lexiques")
     * @ORM\OrderBy({"word" = "ASC"})
     */
    private $listelexique;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $pdf;

    /**
     * @Vich\UploadableField(mapping="labase_pdf", fileNameProperty="pdf")
     * @var File
     */
    private $pdfFile;


    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
        $this->listekeywords = new ArrayCollection();
        $this->listelexique = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCategory(): ?LaBaseCategory
    {
        return $this->category;
    }

    public function setCategory(LaBaseCategory $category): void
    {
        $this->category = $category;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }
    
    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): void
    {
        $this->active = $active;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): void
    {
        $this->author = $author;
    }


    public function getTags(): array
    {
        return $this->tags;
    }

    public function setTags(array $tags): void
    {
        $this->tags = $tags;
    }

    public function getListelexique(): Collection
    {
        return $this->listelexique;
    }

    public function addListelexique(Lexique $lexique): self
    {
        if (!$this->listelexique->contains($lexique)) {
            $this->listelexique[] = $lexique;
        }

        return $this;
    }

    public function removeListelexique(Lexique $lexique): self
    {
        $this->listelexique->removeElement($lexique);

        return $this;
    }

    public function setPdfFile($pdf = null)
    {
        $this->pdfFile = $pdf;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($pdf) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getPdfFile()
    {
        return $this->pdfFile;
    }

    public function setPdf($pdf)
    {
        // $pdf = str_replace(' ', '_', $pdf);
        $this->pdf = $pdf;
    }

    public function getPdf()
    {
        return $this->pdf;
    }

    /**
     * @return Collection<int, Keywords>
     */
    public function getListekeywords(): Collection
    {
        return $this->listekeywords;
    }

    public function addListekeyword(Keywords $listekeyword): self
    {
        if (!$this->listekeywords->contains($listekeyword)) {
            $this->listekeywords[] = $listekeyword;
        }

        return $this;
    }

    public function removeListekeyword(Keywords $listekeyword): self
    {
        $this->listekeywords->removeElement($listekeyword);

        return $this;
    }


}
