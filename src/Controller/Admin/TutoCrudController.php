<?php

namespace App\Controller\Admin;

use App\Entity\Tuto;
use App\Entity\Lexique;
use App\Entity\Keywords;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Vich\UploaderBundle\Form\Type\VichFileType;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;

class TutoCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Tuto::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setDefaultSort(['createdAt' => 'DESC'])
            ->addFormTheme('@FOSCKEditor/Form/ckeditor_widget.html.twig')
            ->setEntityLabelInPlural('La base')
            ->setEntityLabelInSingular('La base')
            ->renderContentMaximized()
            ;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            AssociationField::new('category')
                ->setLabel('Catégorie'),
            TextField::new('title')
                ->setLabel('Titre')
                ->setColumns(6),
            AssociationField::new('author')->hideOnIndex()->hideOnForm(),
            BooleanField::new('active')->setLabel('Affiché sur le site'),
            TextField::new('content')
                ->setFormType(CKEditorType::class)
                ->setLabel('Contenu')
                ->setColumns(8)
                ->hideOnIndex(),
            TextField::new('pdf')->hideOnForm(),
            TextareaField::new('pdfFile')
                ->setLabel('Ajouter un document pdf')
                ->setFormType(VichFileType::class)
                ->hideOnIndex(),
            AssociationField::new('listelexique')
                ->setLabel('Mots à définir')
                ->setHelp('Si le mot que tu veux définir n’est pas dans la liste, ajoute le dans l’onglet Lexique')
                ->setFormTypeOption('choice_label', 'word')
                ->setFormTypeOption('by_reference', false)
                ->hideOnIndex(),
            AssociationField::new('listekeywords')
                ->setLabel('Mots-clés')
                ->setHelp('Si le mot-clé que tu veux ajouter n’est pas dans la liste, ajoute le dans l’onglet Mots-clés')
                ->setFormTypeOption('choice_label', 'keyword')
                ->setFormTypeOption('by_reference', false)
                ->hideOnIndex(),
            DateTimeField::new('createdAt')->hideOnForm()->setLabel('Date')
        ];
    }

    public function configureActions(Actions $actions): Actions
    {
        $actions->add(Crud::PAGE_NEW, Action::INDEX);
        $actions->add(Crud::PAGE_EDIT, Action::INDEX);
        $actions->add(Crud::PAGE_INDEX, Action::DETAIL);

        return $actions;
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add('title')
            ->add('category')
            ->add('active')
            ->add('createdAt')
            ;
    }
}
