<?php

namespace App\Controller;

use App\Entity\Jurisprudence;
use App\Entity\News;
use App\Entity\Tuto;
use App\Entity\Keywords;
use App\Repository\JurisprudenceRepository;
use App\Repository\NewsRepository;
use App\Repository\TutoRepository;
use App\Repository\KeywordsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends AbstractController
{
    private NewsRepository $newsRepository;
    private TutoRepository $tutoRepository;
    private JurisprudenceRepository $jurisprudenceRepository;
    private KeywordsRepository $keywordsRepository;

    public function __construct(
        NewsRepository $newsRepository,
        TutoRepository $tutoRepository,
        JurisprudenceRepository $jurisprudenceRepository,
        KeywordsRepository $keywordsRepository
    )
    {
        $this->newsRepository = $newsRepository;
        $this->tutoRepository = $tutoRepository;
        $this->jurisprudenceRepository = $jurisprudenceRepository;
        $this->keywordsRepository = $keywordsRepository;
    }

    /**
     * @Route("/chercher", name="search")
     */
    public function search(Request $request)
    {
        if (!$request->query->has('search')) {
            return $this->redirectToRoute('homepage');
        }

        $data = [];
        $search = $request->query->get('search');
        $category = $request->query->get('categorie');
        if ('' !== $search) {

            $keywords = $this->keywordsRepository->findByKeyword($search);

            if (null === $category || 'infos' === $category) {
                // Get news
                $news = $this->newsRepository->findNewsByTitle($search);
  
                /** @var News $new */
                foreach ($news as $new) {
                    if ($new->isActive() === true) {
                        $newsData = [
                            'title' => $new->getTitle(),
                            'content' => $this->cleanContent($new->getContent()),
                            'type' => 'news',
                            'id' => $new->getId()
                        ];

                        $data[$new->getCreatedAt()->getTimestamp()] = $newsData;
                    }
                }

                //recherche news par keyword
                foreach ($keywords as $keyword) {
                    $keywordnews = $keyword->getMentionsNews();
                    foreach ($keywordnews as $new) {
                        $news[] = $new;
                    }
                }

                /** @var News $new */
                foreach ($news as $new) {
                    $newsData = [
                        'title' => $new->getTitle(),
                        'content' => $this->cleanContent($new->getContent()),
                        'type' => 'news',
                        'id' => $new->getId()
                    ];

                    // Vérifier que la news n'est pas déjà dans le tableau
                    if (!array_key_exists($new->getCreatedAt()->getTimestamp(), $data) && $new->isActive() === true) {
                        $data[$new->getCreatedAt()->getTimestamp()] = $newsData;
                    }
                }
            }

            if (null === $category || 'tutos' === $category) {
                // Get tutos
                $tutos = $this->tutoRepository->findTutosByTitle($search);
                /** @var Tuto $tuto */
                foreach ($tutos as $tuto) {
                    if ($tuto->isActive()) {
                        $tutoData = [
                            'title' => $tuto->getTitle(),
                            'content' => $this->cleanContent($tuto->getContent()),
                            'type' => 'tuto',
                            'id' => $tuto->getId()
                        ];

                        $data[$tuto->getCreatedAt()->getTimestamp()] = $tutoData;
                    }
                }

                //recherche tutos par keyword
                foreach ($keywords as $keyword) {
                    $keywordtuto = $keyword->getMentionsTutos();
                    foreach ($keywordtuto as $tuto) {
                        $tutos[] = $tuto;
                    }
                }

                /** @var Tuto $tuto */
                foreach ($tutos as $tuto) {
                    $tutoData = [
                        'title' => $tuto->getTitle(),
                        'content' => $this->cleanContent($tuto->getContent()),
                        'type' => 'tuto',
                        'id' => $tuto->getId()
                    ];

                    // Vérifier que le tuto n'est pas déjà dans le tableau + qu'il est active
                    if (!array_key_exists($tuto->getCreatedAt()->getTimestamp(), $data) && $tuto->isActive() === true) {
                        $data[$tuto->getCreatedAt()->getTimestamp()] = $tutoData;
                    }
                }
            }

            if (null === $category || 'jurisprudences' === $category) {
                // Get jp
                $jurisprudences = $this->jurisprudenceRepository->findJPByTitle($search);

                //recherche jp par rg
                $RgJp = $this->jurisprudenceRepository->findJPByRg($search);
                foreach ($RgJp as $jp) {
                    $jurisprudences[] = $jp;
                }

                //recherche jp par keyword
                foreach ($keywords as $keyword) {
                    $keywordjp = $keyword->getMentionsJp();
                    foreach ($keywordjp as $jp) {
                        $jurisprudences[] = $jp;
                    }
                }

                /** @var Jurisprudence $jurisprudence */
                foreach ($jurisprudences as $jurisprudence) {
                    $jpData = [
                        'title' => $jurisprudence->getName(),
                        'content' => $this->cleanContent($jurisprudence->getContent()),
                        'type' => 'jurisprudence',
                        'id' => $jurisprudence->getId()
                    ];

                    // Vérifier que la JP n'est pas déjà dans le tableau
                    if (!array_key_exists($jurisprudence->getCreatedAt()->getTimestamp(), $data) && $jurisprudence->isActive() === true) {
                        $data[$jurisprudence->getCreatedAt()->getTimestamp()] = $jpData;
                    }
                }
            }
        }

        ksort($data);

        return $this->render('search/search.html.twig', [
            'controller_name' => 'HomepageController',
            'active_menu' => 'homepage',
            'dataContent' => $data,
            'categoryRequest' => $category,
            'search' => $search
        ]);
    }


    private function cleanContent(string $content)
    {
        $contentClean = strip_tags($content, '<br/><br>');
        $contentClean = str_replace("&rsquo;", "'", $contentClean);
        $contentClean = str_replace("&#39;", "'", $contentClean);

        return $contentClean;
    }
}
