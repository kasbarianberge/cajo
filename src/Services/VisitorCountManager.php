<?php

namespace App\Services;

use App\Entity\VisitorCount;
use App\Entity\Jurisprudence;
use App\Entity\News;
use App\Entity\Tuto;
use App\Repository\VisitorCountRepository;
use Doctrine\ORM\EntityManagerInterface;

class VisitorCountManager
{
    private EntityManagerInterface $entityManager;
    private VisitorCountRepository $visitorCountRepository;

    public function __construct(EntityManagerInterface $entityManager, VisitorCountRepository $visitorCountRepository)
    {
        $this->entityManager = $entityManager;
        $this->visitorCountRepository = $visitorCountRepository;
    }

    /**
     * @param mixed $object
     */
    public function incrementStats($object)
    {
        $className = get_class($object);
        $visitorCount = $this->visitorCountRepository->findOneBy([
            'category' => $className,
            'pageId' => $object->getId()
        ]);

        if ($visitorCount) {
            $visitorCount->setCounter($visitorCount->getCounter() + 1);
        } else {
            $visitorCount = new VisitorCount();
            $visitorCount->setType(VisitorCount::ARTICLE_READ_TYPE);
            $visitorCount->setCounter(1);
            $visitorCount->setCategory($className);
            $visitorCount->setPageId($object->getId());
        }

        $this->entityManager->persist($visitorCount);
        $this->entityManager->flush();
    }

    public function resetStats()
    {
        $stats = $this->visitorCountRepository->findAll();
        foreach ($stats as $stat)  {
            $this->entityManager->remove($stat);
        }

        $this->entityManager->flush();
    }

    public function updateStats()
    {
        $stats = $this->visitorCountRepository->findAll();

        foreach ($stats as $stat) {
            $page = $this->entityManager->getRepository($stat->getCategory())->find($stat->getPageId());

            if (!$page instanceof Jurisprudence && !$page instanceof Tuto && !$page instanceof News) {
                $this->entityManager->remove($stat);
            }
        }
    }
}