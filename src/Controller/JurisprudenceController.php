<?php

namespace App\Controller;

use App\Entity\JurisprudenceCategory;
use App\Repository\JurisprudenceCategoryRepository;
use App\Repository\JurisprudenceRepository;
use App\Services\VisitorCountManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class JurisprudenceController extends AbstractController
{
    private JurisprudenceCategoryRepository $jurisprudenceCategoryRepository;
    private JurisprudenceRepository $jurisprudenceRepository;
    private VisitorCountManager $visitorCountManager;

    public function __construct(
        JurisprudenceCategoryRepository $jurisprudenceCategoryRepository,
        JurisprudenceRepository $jurisprudenceRepository,
        VisitorCountManager $visitorCountManager
    ) {
        $this->jurisprudenceCategoryRepository = $jurisprudenceCategoryRepository;
        $this->jurisprudenceRepository = $jurisprudenceRepository;
        $this->visitorCountManager = $visitorCountManager;
    }

    /**
     * @Route("/trouver-une-jurisprudence", name="jurisprudences_search")
     */
    public function jurisprudences(Request $request): Response
    {
        $jpData = [];
        $jurisprudencesAll = $this->jurisprudenceCategoryRepository->findBy([
            'enabled' => true
        ]);

        // Everything in php because the first version need too many SQL requests (14sec doctrine debug)
        /** @var JurisprudenceCategory $jurisprudenceCategory */
        foreach ($jurisprudencesAll as $jurisprudenceCategory) {
            if (null === $jurisprudenceCategory->getParent()) {
                $jpData[0][] = [
                    'id' => $jurisprudenceCategory->getId(),
                    'name' => $jurisprudenceCategory->getName(),
                    'enabled' => true,
                    'parentId' => null,
                    'createdAt' => $jurisprudenceCategory->getCreatedAt(),
                ];
            }
        }

        for ($i=1;$i<30;$i++) {
            /** @var JurisprudenceCategory $jurisprudenceCategory */
            foreach ($jurisprudencesAll as $jurisprudenceCategory) {

                // Category has no parent, stop iteration
                if (null === $jurisprudenceCategory->getParent()) {
                    continue;
                }

                // Previous iteration does not exist, stop the loop
                if (!array_key_exists($i-1, $jpData)) {
                    break;
                }

                if (array_search($jurisprudenceCategory->getParent()->getId(), array_column($jpData[$i-1], 'id')) !== false) {
                    $jpData[$i][] = [
                        'id' => $jurisprudenceCategory->getId(),
                        'name' => $jurisprudenceCategory->getName(),
                        'enabled' => true,
                        'parentId' => $jurisprudenceCategory->getParent()->getId(),
                        'createdAt' => $jurisprudenceCategory->getCreatedAt(),
                    ];
                }
            }
        }

        return $this->render('jurisprudence/default.html.twig', [
            'controller_name' => 'JurisprudenceController',
            'active_menu' => 'jp',
            'jpData' => $jpData,
        ]);
    }

    /**
     * @Route("/jurisprudences-liste", name="jurisprudences_list")
     */
    public function jurisprudencesList(Request $request): Response
    {
        if (!$request->query->has('categorie')) {
            return new Response();
        }

        $category = $this->jurisprudenceCategoryRepository->find((int) $request->query->get('categorie'));
        $jurisprudences=[];
        $children=[];
        $test=[];

        if ($category === null) {
            //si la requête est : toutes les jurisprudences
            $jurisprudences = $this->jurisprudenceRepository->findBy([
                'active' => true
            ], [
                'important' => 'DESC',
                'createdAt' => 'DESC'
            ]);
        } else {
            //sinon, chercher tous les catégories descendantes de la catégorie demandée (sur 1+5 générations)
            $children[] = $category;
            $test[0] = [];
            $directChildren = $category->getChildrens();
            foreach ($directChildren as $child) {
                $test[0][] = $child;
                $children[] = $child;
            }
            for ($i=0; $i < 8; $i++) { 
                if (sizeof($test[$i]) == 0) {
                break;
                }

                $test[$i+1] = [];
                foreach ($test[$i] as $child) {
                    $poufs = $child->getChildrens();
                    foreach ($poufs as $pouf) {
                        $children[] = $pouf;
                        $test[$i+1][] = $pouf;
                    }
                }
            }

            $jurisprudences = $this->jurisprudenceRepository->FindJPByCategory($children);
            //et chercher les jp présentes dans toutes ces catégories
            /*$jurisprudences = $this->jurisprudenceRepository->findBy([
                'active' => true,
                'category' => $children
            ], [
                'important' => 'ASC',
                'createdAt' => 'DESC'

            ]);*/
            /*foreach ($children as $child) {
                $jps = $child->getJps();
                foreach ($jps as $jp) {
                    if (!array_key_exists($jp->getCreatedAt()->getTimestamp(), $jurisprudences) && $jp->isActive() === true) {
                        $jurisprudences[$jp->getCreatedAt()->getTimestamp()] = $jp;
                    }
                }
            }*/
        }

        $jurisprudencesClean = [];
        foreach ($jurisprudences as $jurisprudence) {
            $content = $jurisprudence->getContent();
            $contentClean = strip_tags($content, '<br/><br>');
            $contentClean = str_replace("&rsquo;", "'", $contentClean);
            $contentClean = str_replace("&#39;", "'", $contentClean);
            $contentClean = str_replace("<br><br>", "<br>", $contentClean);
            $jurisprudence->setContent($contentClean);
            $jurisprudencesClean[] = $jurisprudence;
        }

        return $this->render('jurisprudence/list.html.twig', [
            'jurisprudences' => $jurisprudencesClean,
            'category' => $category,
        ]);
    }

    /**
     * @Route("/jurisprudence/{jurisprudenceId}", name="jurisprudence_show")
     */
    public function jurisprudenceShow(string $jurisprudenceId): Response
    {
        $jurisprudence = $this->jurisprudenceRepository->find((int) $jurisprudenceId);
        if (null === $jurisprudence) {
            throw new NotFoundHttpException();
        }


        //Obtenir les catégories parentes de la jurisprudence
        $jpData = [];
        $i=1;
        $jpCategoriesId = $jurisprudence->getCategory();
        foreach ($jpCategoriesId as $category) {
            $jpData[$i][] = [
                'name' => $category->getName(),
            ];
            $parent = $category;
            while (null != $parent->getParent()) {
                $jpData[$i][] = [
                    'name' => $parent->getParent()->getName(),
                ];
                $parent = $parent->getParent();
            }
            $jpData[$i] = array_reverse($jpData[$i]);     
            $i=$i+1;
        }
          

        // Count visitors
        $this->visitorCountManager->incrementStats($jurisprudence);


        return $this->render('jurisprudence/show.html.twig', [
            'jurisprudence' => $jurisprudence,
            //'jpCategory' => $jurisprudenceCategory,
            'jpData' => $jpData,
            'active_menu' => 'jp', 
        ]);
    }

    /**
     * @Route("/jurisprudences-mot-cle/{tag}", name="jurisprudences_by_tag")
     */
    public function jurisprudencesByTag(string $tag): Response
    {
        $jurisprudences = $this->jurisprudenceRepository->findJurisprudencesByTag($tag);

        return $this->render('jurisprudence/jurisprudences_by_tag.html.twig', [
            'jurisprudences' => $jurisprudences,
            'active_menu' => 'jp',
            'tag' => $tag
        ]);
    }
}