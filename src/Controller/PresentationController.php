<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PresentationController extends AbstractController
{
    // private EntityManagerInterface $entityManager;
    //
    // public function __construct(EntityManagerInterface $entityManager)
    // {
    //     $this->entityManager = $entityManager;
    // }

    /**
     * @Route("/presentation", name="presentation")
     */
    public function presentation(Request $request): Response
    {

        return $this->render('presentation/presentation.html.twig', [
            'controller_name' => 'PresentationController',
            'active_menu' => 'presentation',
        ]);
    }
}
