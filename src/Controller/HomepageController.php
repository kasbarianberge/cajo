<?php

namespace App\Controller;

use App\Entity\News;
use App\Entity\VisitorCount;
use App\Repository\NewsRepository;
use App\Repository\TutoRepository;
use App\Repository\JurisprudenceRepository;
use App\Repository\VisitorCountRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\Paginator;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomepageController extends AbstractController
{
    private JurisprudenceRepository $jurisprudenceRepository;
    private NewsRepository $newsRepository;
    private TutoRepository $tutoRepository;
    private VisitorCountRepository $visitorCountRepository;
    private EntityManagerInterface $entityManager;

    public function __construct(
        JurisprudenceRepository $jurisprudenceRepository,
        NewsRepository $newsRepository,
        TutoRepository $tutoRepository,
        VisitorCountRepository $visitorCountRepository,
        EntityManagerInterface $entityManager
    ) {
        $this->jurisprudenceRepository = $jurisprudenceRepository;
        $this->newsRepository = $newsRepository;
        $this->tutoRepository = $tutoRepository;
        $this->visitorCountRepository = $visitorCountRepository;
        $this->entityManager = $entityManager;
    }


    /**
     * @Route("/", name="default")
     */
    public function default(): Response
    {
        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/accueil", name="homepage")
     */
    public function index(Request $request, PaginatorInterface $paginator): Response
    {
        // Stats total
        $totalCount = $this->visitorCountRepository->findOneBy(['type' => VisitorCount::TOTAL_VISITORS_TYPE]);
        if ($totalCount) {
            $totalCount->setCounter($totalCount->getCounter() + 1);
        } else {
            $totalCount = new VisitorCount();
            $totalCount->setType(VisitorCount::TOTAL_VISITORS_TYPE);
            $totalCount->setCounter(1);
            $this->entityManager->persist($totalCount);
        }
        $this->entityManager->flush();

        $data = [];
        $category = $request->query->get('categorie');


        //Get news
        if (null === $category || 'infos' === $category ) {
            $news = $this->newsRepository->findBy(['active' => true]);
            /** @var News $new */
            foreach ($news as $new) {
                $newsData = [
                    'title' => $new->getTitle(),
                    'content' => $this->cleanContent($new->getContent()),
                    'type' => 'news',
                    'id' => $new->getId()
                ];
                $data[$new->getCreatedAt()->getTimestamp()] = $newsData;
            }
        }

        // Get tutos
        if(null === $category || 'tutos' === $category) {
            $tutos = $this->tutoRepository->findBy(['active' => true]);
            /** @var Tuto $tuto */
            foreach ($tutos as $tuto) {
                $tutoData = [
                    'title' => $tuto->getTitle(),
                    'content' => $this->cleanContent($tuto->getContent()),
                    'type' => 'tuto',
                    'id' => $tuto->getId()
                ];

                $data[$tuto->getCreatedAt()->getTimestamp()] = $tutoData;
            }
        }

        // Get jp
        if(null === $category || 'jurisprudences' === $category) {
            $jurisprudences = $this->jurisprudenceRepository->findBy(['active' => true]);
            /** @var Jurisprudence $jurisprudence */
            foreach ($jurisprudences as $jurisprudence) {
                $jpData = [
                    'title' => $jurisprudence->getName(),
                    'content' => $this->cleanContent($jurisprudence->getContent()),
                    'type' => 'jurisprudence',
                    'id' => $jurisprudence->getId()
                ];

                $data[$jurisprudence->getCreatedAt()->getTimestamp()] = $jpData;
            }
        }

        krsort($data);

        $pagination = $paginator->paginate($data, $request->query->get('page', 1), 6);

        return $this->render('homepage/homepage_default.html.twig', [
            'controller_name' => 'HomepageController',
            'active_menu' => 'homepage',
            'dataContent' => $pagination,
            'categoryRequest' => $category,
            //'search' => $search
        ]);

    }

    private function cleanContent(string $content)
    {
        $contentClean = strip_tags($content, '<br/><br>');
        $contentClean = str_replace("&rsquo;", "'", $contentClean);
        $contentClean = str_replace("&#39;", "'", $contentClean);
        $contentClean = str_replace("<br><br>", "<br>", $contentClean);

        return $contentClean;
    }

}
