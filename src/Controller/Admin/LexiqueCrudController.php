<?php

namespace App\Controller\Admin;

use App\Entity\Lexique;
use App\Entity\News;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;

class LexiqueCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Lexique::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setDefaultSort(['word' => 'ASC'])
            ->setEntityLabelInPlural('Mots à définir')
            ->setEntityLabelInSingular('Mot à définir');
    }
    
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            TextField::new('word')->setLabel('Mot à définir'),
            BooleanField::new('active')->setLabel('Affiché sur le site'),
            TextEditorField::new('definition')
                ->setLabel('Définition')
                ->hideOnIndex()
                ->setColumns(8)
                ->setNumOfRows(10),
            DateTimeField::new('createdAt')->hideOnForm()->setLabel('Date')
        ];
    }

    public function configureActions(Actions $actions): Actions
    {
        $actions->add(Crud::PAGE_NEW, Action::INDEX);
        $actions->add(Crud::PAGE_EDIT, Action::INDEX);
        $actions->add(Crud::PAGE_INDEX, Action::DETAIL);

        return $actions;
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add('word')
            ->add('active')
            ->add('createdAt')
            ;
    }

}
