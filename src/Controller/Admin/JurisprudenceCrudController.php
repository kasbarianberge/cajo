<?php
namespace App\Controller\Admin;

use App\Entity\Jurisprudence;
use App\Entity\Lexique;
use App\Entity\Keywords;
use App\Entity\JurisprudenceCategory;
use App\Repository\JurisprudenceCategoryRepository;
use Symfony\Component\HttpFoundation\RequestStack;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Doctrine\Common\Collections\ArrayCollection;

class JurisprudenceCrudController extends AbstractCrudController
{
    private $requestStack;

    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }
    
    public static function getEntityFqcn(): string
    {
        return Jurisprudence::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setDefaultSort(['createdAt' => 'DESC'])
            ->addFormTheme('@FOSCKEditor/Form/ckeditor_widget.html.twig')
            ->setEntityLabelInPlural('Jurisprudences')
            ->setEntityLabelInSingular('Jurisprudence')
            ;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            AssociationField::new('category')
                ->setLabel('Catégorie')
                ->setHelp('Pour être sûr.e d’ajouter ta jp dans la bonne catégorie, navigue dans catégories depuis le site public puis clique sur Ajouter une jurisprudence')
                ->setFormTypeOption('by_reference', false)
                ->setFormTypeOption('multiple', true),
            TextField::new('name')
                ->setLabel('Nom')
                ->setColumns(6),
            BooleanField::new('active')->setLabel('Affichée sur le site'),
            AssociationField::new('author')->hideOnIndex()->hideOnForm(),
            TextField::new('pdf')->hideOnForm(),
            TextareaField::new('pdfFile')
                ->setLabel('Joindre la jp en pdf')
                ->setHelp('S’assurer que la jp ait bien été anonymisée avec l’outil Nettoyeur de métadonnées')
                ->setFormType(VichFileType::class)
                ->hideOnIndex(),
            TextEditorField::new('content')
                //->setFormType(CKEditorType::class)
                ->setLabel('Contenu')
                ->setColumns(8)
                ->setNumOfRows(10)
                ->hideOnIndex(),
            TextField::new('rg')
                ->setLabel('Numéro de RG')
                ->setHelp('Merci de le mettre au format XX/XXXXX')
                ->setColumns(5),
            ChoiceField::new('quality')->setLabel('Favorable')->hideOnIndex()->setChoices([
                'Favorable' => 'good',
                'Défavorable' => 'bad',
                "Ni l'un ni l'autre" => null
            ]),
            TextField::new('justiceCourt')
            ->setLabel('Cour de justice')
            ->setColumns(6)
            ->hideOnIndex(),
            DateField::new('date')->setLabel('Date de la jurisprudence'),
            BooleanField::new('important')
                ->setLabel('Jurisprudence importante')
                ->setHelp('Les jurisprudences importantes sont affichées en priorité.'),
            AssociationField::new('listelexique')
                ->setLabel('Mots à définir')
                ->setHelp('Si le mot que tu veux définir n’est pas dans la liste, ajoute le dans l’onglet Lexique')
                ->setFormTypeOption('choice_label', 'word')
                ->setFormTypeOption('by_reference', false)
                ->hideOnIndex(),
            AssociationField::new('listekeywords')
                ->setLabel('Mots-clés')
                ->setHelp('Si le mot-clé que tu veux ajouter n’est pas dans la liste, ajoute le dans l’onglet Mots-clés')
                ->setFormTypeOption('choice_label', 'keyword')
                ->setFormTypeOption('by_reference', false)
                ->hideOnIndex(),
            DateTimeField::new('createdAt')->hideOnForm()->setLabel('Ajoutée le')
        ];
    }

    public function createEntity(string $entityFqcn)
    {
        $entity = new $entityFqcn();

        $request = $this->requestStack->getCurrentRequest();
        $cat = $request->query->get('category');
        /*if (is_string($cat)) {
            $cat = explode(',', $cat);
        }*/
        if (null != $cat) {
            $category = $this->getDoctrine()->getRepository(JurisprudenceCategory::class)->find((int) $cat);
            $entity->addCategory($category);

        }

        return $entity;
    }


    public function configureActions(Actions $actions): Actions
    {
        $actions->add(Crud::PAGE_NEW, Action::INDEX);
        $actions->add(Crud::PAGE_EDIT, Action::INDEX);
        $actions->add(Crud::PAGE_INDEX, Action::DETAIL);

        return $actions;
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add('category')
            ->add('name')
            ->add('date')
            ;
    }
}
