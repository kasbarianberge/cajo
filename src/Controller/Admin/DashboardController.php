<?php

namespace App\Controller\Admin;

use App\Entity\Contact;
use App\Entity\Contribution;
use App\Entity\Jurisprudence;
use App\Entity\JurisprudenceCategory;
use App\Entity\News;
use App\Entity\NewsCategory;
use App\Entity\Tuto;
use App\Entity\User;
use App\Entity\LaBaseCategory;
use App\Entity\Lexique;
use App\Entity\Keywords;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
	#public function __construct(
	#	private AdminUrlGenerator $adminUrlGenerator
	#) {
	#}
    /**
     * @Route("/secret", name="admin")
     */
    #public function index(): Response
    #{
    #    $routeBuilder = $this->get(CrudUrlGenerator::class)->build();

    #    return $this->redirect($routeBuilder->setController(NewsCrudController::class)->generateUrl());
	#}
	public function index(): Response
	{
        $routeBuilder = $this->get(AdminUrlGenerator::class);
        return $this->redirect($routeBuilder->setController(NewsCrudController::class)->generateUrl());
		#$url = $this->AdminUrlGenerator
		#	  ->setController(SyncJobCrudController::class)
		#	  ->setAction('index')
		#	  ->generateUrl();

		#return $this->redirect($url);
		#return parent::index();
	}

    public function configureCrud(): Crud
    {
        return Crud::new()
            ->setPaginatorPageSize(30)
            ->setDateFormat('medium')
            ->setDateTimeFormat('medium')
            ;
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('CAJO')
            ->disableUrlSignatures()
            ;
    }

    public function configureMenuItems(): iterable
    {
        return [
            MenuItem::section('Users')
                ->setPermission('ROLE_ADMIN'),
            MenuItem::linkToCrud("Utilisateurs", 'fa fa-user', User::class)
                ->setPermission('ROLE_ADMIN'),

            MenuItem::section('Actu'),
            MenuItem::linkToCrud("Catégorie d'actu", 'fa fa-newspaper', NewsCategory::class),
            MenuItem::linkToCrud('Actu', 'fa fa-newspaper', News::class),

            MenuItem::section('Jurisprudence'),
            MenuItem::linkToCrud('Type de Jurisprudence', 'fa fa-balance-scale', JurisprudenceCategory::class),
            MenuItem::linkToCrud('Jurisprudence', 'fa fa-balance-scale', Jurisprudence::class),

            MenuItem::section('La Base'),
            MenuItem::linkToCrud('Catégories dans La Base','fa fa-list-alt', LaBaseCategory::class),
            MenuItem::linkToCrud('La Base', 'fa fa-list-alt', Tuto::class),
            MenuItem::linkToCrud('Lexique','fa fa-book', Lexique::class),
            
            MenuItem::section('Mots-clés'),
            MenuItem::linkToCrud('Mots clés', 'fa fa-key', Keywords::class),

            MenuItem::section('Contributions'),
            MenuItem::linkToCrud('Contributions reçues', 'fa fa-envelope-open-text', Contribution::class),

            MenuItem::section('Contact'),
            MenuItem::linkToCrud('Messages reçus', 'fa fa-envelope-square', Contact::class),

            MenuItem::section('Liens externes'),
            MenuItem::linktoUrl('Cryptdrive', 'fa fa-link', 'https://cryptpad.fr/drive/#/2/drive/edit/5Z5cR7uFbRjLYeJqtcqk3Nhs/p/')
        ];
    }
}
