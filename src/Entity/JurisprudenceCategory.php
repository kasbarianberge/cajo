<?php

namespace App\Entity;

use App\Repository\JurisprudenceCategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use phpDocumentor\Reflection\PseudoTypes\False_;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=JurisprudenceCategoryRepository::class)
 */
class JurisprudenceCategory
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank
     * @ORM\Column(type="string", length=255)
     */
    private string $name;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $enabled = false;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?\Datetime $createdAt;

    /**
     * @var JurisprudenceCategory
     * @ORM\ManyToOne(targetEntity=JurisprudenceCategory::class, inversedBy="childrens")
     * @ORM\JoinColumn(nullable="true", onDelete="SET NULL")
     */
    private ?JurisprudenceCategory $parent;

    /**
     * @var JurisprudenceCategory[]
     * @ORM\OneToMany(targetEntity=JurisprudenceCategory::class, mappedBy="parent")
     */
    private $childrens;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Jurisprudence", mappedBy="category")
     */
    private $jps;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->childrens = new ArrayCollection();
        $this->jps = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getChildrens(): Collection
    {
        return $this->childrens;
    }

    public function addChildren(self $children): self
    {
        if (!$this->childrens->contains($children)) {
            $this->childrens[] = $children;
            $children->setParent($this);
        }

        return $this;
    }

    public function removeChildren(self $children): self
    {
        if ($this->childrens->removeElement($children)) {
            // set the owning side to null (unless already changed)
            if ($children->getParent() === $this) {
                $children->setParent(null);
            }
        }

        return $this;
    }

    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): void
    {
        $this->enabled = $enabled;
    }

    public function __toString()
    {
        return '#'.$this->id.' - '.$this->name;
    }

    /**
     * @return Collection<int, Jurisprudence>
     */
    public function getJps(): Collection
    {
        return $this->jps;
    }

    public function addJp(Jurisprudence $jp): self
    {
        if (!$this->jps->contains($jp)) {
            $this->jps[] = $jp;
            $jp->addCategory($this);
        }

        return $this;
    }

    public function removeJp(Jurisprudence $jp): self
    {
        if ($this->jps->removeElement($jp)) {
            $jp->removeCategory($this);
        }

        return $this;
    }
}
