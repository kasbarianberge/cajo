<?php

namespace App\Controller;

use App\Entity\News;
use App\Entity\Tuto;
use App\Entity\Jurisprudence;
use App\Entity\VisitorCount;
use App\Repository\VisitorCountRepository;
use App\Services\VisitorCountManager;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class StatsController extends AbstractController
{
    private VisitorCountManager $visitorCountManager;
    private VisitorCountRepository $visitorCountRepository;
    private EntityManagerInterface $entityManager;

    public function __construct(VisitorCountManager $visitorCountManager, 
        VisitorCountRepository $visitorCountRepository, 
        EntityManagerInterface $entityManager)
    {
        $this->visitorCountManager = $visitorCountManager;
        $this->visitorCountRepository = $visitorCountRepository;
        $this->entityManager = $entityManager;
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     * @Route("/statistiques", name="stats")
     */
    public function statistics()
    {
        $statsData = [];
        $visitorsCount = $this->visitorCountRepository->findBy([
            'type' => VisitorCount::ARTICLE_READ_TYPE
        ], [
            'counter' => 'DESC'
        ], 5);

        foreach ($visitorsCount as $count) {
            $page = $this->entityManager->getRepository($count->getCategory())->find($count->getPageId());

            if ($page instanceof Tuto || $page instanceof News) {
                $title = $page->getTitle();
            } elseif ($page instanceof Jurisprudence) {
                $title = $page->getName();
            } else {
                $title = 'Cet article a été supprimé';
            }

            $statsData[] = [
                'title' => $title,
                'category' => $count->getCategory(),
                'count' => $count->getCounter(),
                'id' => $count->getId(),
                'pageId' => $count->getPageId()
            ];
        }

        return $this->render('stats/stats_consulting.html.twig', [
            'controller_name' => 'StatsController',
            'active_menu' => '',
            'stats' => $statsData,
            'total_visitors' => $this->visitorCountRepository->findOneBy(['type' => VisitorCount::TOTAL_VISITORS_TYPE])
        ]);
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     * @Route("/reset-stats", name="reset_stats")
     */
    public function resetStats()
    {
        $this->visitorCountManager->resetStats();
        return $this->redirectToRoute('stats');
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     * @Route("/update-stats", name="update_stats")
     */
    public function updateStats()
    {
        $this->visitorCountManager->updateStats();
        return $this->redirectToRoute('stats');
    }
}
