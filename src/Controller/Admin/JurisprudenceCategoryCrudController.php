<?php

namespace App\Controller\Admin;

use App\Entity\JurisprudenceCategory;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\HttpFoundation\RequestStack;

class JurisprudenceCategoryCrudController extends AbstractCrudController
{
    private $requestStack;

    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    public static function getEntityFqcn(): string
    {
        return JurisprudenceCategory::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setDefaultSort(['createdAt' => 'DESC'])
            ->setEntityLabelInPlural('Catégories des jurisprudences')
            ->setEntityLabelInSingular('Catégorie de jurisprudences');
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            TextField::new('name')->setLabel('Nom'),
            BooleanField::new('enabled')->setLabel('Catégorie active'),
            AssociationField::new('parent')
            ->setLabel('Catégorie parente')
            ->setHelp('Pour être sûr.e d’ajouter ta catégorie dans la bonne catégorie, navigue dans catégories depuis le site public puis clique sur Ajouter une catégorie'),
            DateTimeField::new('createdAt')->hideOnForm()->setLabel('Ajoutée le')
        ];
    }

    public function createEntity(string $entityFqcn)
    {
        $entity = new $entityFqcn();

        $request = $this->requestStack->getCurrentRequest();
        $cat = $request->query->get('category');
        if (null != $cat) {
            $parent = $this->getDoctrine()->getRepository(JurisprudenceCategory::class)->find((int) $cat);
            $entity->setParent($parent);
        }

        return $entity;
    }

    public function configureActions(Actions $actions): Actions
    {
        $actions->add(Crud::PAGE_NEW, Action::INDEX);
        $actions->add(Crud::PAGE_EDIT, Action::INDEX);
        $actions->add(Crud::PAGE_INDEX, Action::DETAIL);

        return $actions;
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add('name')
            ->add('enabled')
            ->add('createdAt')
            ;
    }
}
