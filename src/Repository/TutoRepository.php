<?php

namespace App\Repository;

use App\Entity\Tuto;
use App\Entity\LaBaseCategory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Tuto|null find($id, $lockMode = null, $lockVersion = null)
 * @method Tuto|null findOneBy(array $criteria, array $orderBy = null)
 * @method Tuto[]    findAll()
 * @method Tuto[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TutoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Tuto::class);
    }

    public function findActiveTutosQuery()
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.active = :active')
            ->setParameter('active', true)
            ->orderBy('t.createdAt', 'DESC')
            ->getQuery()
        ;
    }

    public function findTutoActiveQueryByCategory(LaBaseCategory $category = null)
    {
        $queryBuilder = $this->createQueryBuilder('n')
            ->andWhere('n.active = :active')
            ->setParameter('active', true)
            ->orderBy('n.createdAt', 'DESC');

        if (null !== $category) {
            $queryBuilder->andWhere('n.category = :category')
                ->setParameter('category', $category->getId());
        }

        return $queryBuilder->getQuery();
    }


    public function findTutosByTitle(?string $title = null)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.active = :active')
            ->andWhere('t.title LIKE :title')
            ->setParameter('active', true)
            ->setParameter('title', '%'.$title.'%')
            ->orderBy('t.createdAt', 'DESC')
            ->getQuery()
            ->getResult()
            ;
    }


    public function findTutosByTag(string $tag)
    {
        return $this->createQueryBuilder('t')
            ->where('t.tags LIKE :tags')
            ->setParameter('tags', '%"'.$tag.'"%')
            ->getQuery()
            ->getResult();
    }
}
