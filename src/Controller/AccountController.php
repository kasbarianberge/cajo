<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
//use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AccountController extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private userPasswordHasherInterface $userPasswordHasherInterface;

    public function __construct(EntityManagerInterface $entityManager, userPasswordHasherInterface $userPasswordHasherInterface)
    {
        $this->entityManager = $entityManager;
        $this->userPasswordHasherInterface = $userPasswordHasherInterface;
    }

    /**
     * @Route("/mon-compte", name="my_account")
     */
    public function myAccount(Request $request)
    {
        $me = $this->getUser();

        $form = $this->createFormBuilder($me)
            ->add('email', TextType::class, [
                'label' => "Email (c'est l'adresse qui servira à vous connecter)",
            ])
            ->add('pseudo', TextType::class, [
                'required' => false
            ])
            ->add('plainPassword', PasswordType::class, [
                'required' => false,
                'label' => 'Changez votre de passe',
                'empty_data' => ''
            ])
            ->add('save', SubmitType::class, ['label' => 'Mettre à jour mes informations'])
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            $plainPassword = $user->getPlainPassword();

            if ('' !== $plainPassword) {
                $user->setPassword(
                    $this->userPasswordHasherInterface->hashPassword(
                        $user,
                        $plainPassword
                    )
                );
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('my_account');
        }

        return $this->render('account/my_account.html.twig', [
            'active_menu' => '',
            'news' => $me,
            'form' => $form->createView()
        ]);
    }
}
