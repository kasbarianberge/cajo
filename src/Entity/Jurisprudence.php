<?php

namespace App\Entity;

use App\Repository\JurisprudenceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Entity\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=JurisprudenceRepository::class)
 * @Vich\Uploadable
 */
class Jurisprudence
{
    const QUALITY_NONE = null;
    const QUALITY_GOOD = 'good';
    const QUALITY_BAD = 'bad';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $active = false;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=true)
     */
    private ?User $author;

    /**
     * @Assert\NotBlank
     * @ORM\Column(type="string", length=255)
     */
    private string $name;

    /**
     * @Assert\NotBlank
     * @ORM\Column(type="text")
     */
    private string $content;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?\Datetime $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?\Datetime $updatedAt;

    /**
     * @Assert\NotNull
     * @ORM\ManyToMany(targetEntity="App\Entity\JurisprudenceCategory", inversedBy="jps", cascade={"persist"})
     * @ORM\JoinTable(name="jp_categories")
     * @ORM\OrderBy({"name" = "ASC"})
     */
    private $category;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private \DateTime $date;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $pdf;

    /**
     * @Vich\UploadableField(mapping="juriprudences_pdf", fileNameProperty="pdf")
     * @var File
     */
    private $pdfFile;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     * @var string
     */
    private $quality = self::QUALITY_NONE;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $justiceCourt;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Keywords", inversedBy="mentionsJp", cascade={"persist"})
     * @ORM\JoinTable(name="jp_keywords")
     * @ORM\OrderBy({"keyword" = "ASC"})
     */
    private $listekeywords;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Lexique", inversedBy="mentionsJp", cascade={"persist"})
     * @ORM\JoinTable(name="jp_lexiques")
     * @ORM\OrderBy({"word" = "ASC"})
     */
    private $listelexique;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * 
     */
    private $rg;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $important = false;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
        $this->date = new \DateTime();
        $this->listekeywords = new ArrayCollection();
        $this->listelexique = new ArrayCollection();
        $this->category = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function setCategory(Collection $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): void
    {
        $this->active = $active;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): void
    {
        $this->author = $author;
    }

    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    public function setPdfFile($pdf = null)
    {
        $this->pdfFile = $pdf;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($pdf) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getPdfFile()
    {
        return $this->pdfFile;
    }

    public function setPdf($pdf)
    {
        // $pdf = str_replace(' ', '_', $pdf);
        $this->pdf = $pdf;
    }

    public function getPdf()
    {
        return $this->pdf;
    }

    public function getQuality(): ?string
    {
        return $this->quality;
    }

    public function setQuality(?string $quality): void
    {
        $this->quality = $quality;
    }

    public function getJusticeCourt(): ?string
    {
        return $this->justiceCourt;
    }

    public function setJusticeCourt(?string $justiceCourt): void
    {
        $this->justiceCourt = $justiceCourt;
    }

    public function getListelexique(): Collection
    {
        return $this->listelexique;
    }

    public function addListelexique(Lexique $lexique): self
    {
        if (!$this->listelexique->contains($lexique)) {
            $this->listelexique[] = $lexique;
        }

        return $this;
    }

    public function removeListelexique(Lexique $lexique): self
    {
        $this->listelexique->removeElement($lexique);

        return $this;
    }

    /**
     * @return Collection<int, Keywords>
     */
    public function getListekeywords(): Collection
    {
        return $this->listekeywords;
    }

    public function addListekeyword(Keywords $listekeyword): self
    {
        if (!$this->listekeywords->contains($listekeyword)) {
            $this->listekeywords[] = $listekeyword;
        }

        return $this;
    }

    public function removeListekeyword(Keywords $listekeyword): self
    {
        $this->listekeywords->removeElement($listekeyword);

        return $this;
    }

    public function getCategory(): Collection
    {
        return $this->category;
    }


    public function addCategory(JurisprudenceCategory $category): self
    {
        if (!$this->category->contains($category)) {
            $this->category[] = $category;
        }

        return $this;
    }

    public function removeCategory(JurisprudenceCategory $category): self
    {
        $this->category->removeElement($category);

        return $this;
    }

    public function getRg(): ?string
    {
        return $this->rg;
    }

    public function setRg(string $rg): self
    {
        $this->rg = $rg;

        return $this;
    }

    public function isImportant(): ?bool
    {
        return $this->important;
    }

    public function setImportant(bool $important): self
    {
        $this->important = $important;

        return $this;
    }

}
