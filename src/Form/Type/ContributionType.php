<?php

namespace App\Form\Type;

use App\Entity\Contribution;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Gregwar\CaptchaBundle\Type\CaptchaType;
use Vich\UploaderBundle\Form\Type\VichImageType;


class ContributionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Titre de la contribution'
            ])
            ->add('type', ChoiceType::class, [
                'label' => 'Type',
                'choices'  => [
                    'Info' => 'info',
                    'Tuto' => 'tuto',
                    'Jurisprudence' => 'jurisprudence',
                    'Autre' => 'autre',
                ],
            ])

            ->add('content', CKEditorType::class, [
                'attr' => array('rows' => '10'),
                'label' => 'Contenu de votre contribution'
            ])
            
            ->add('pdfFile', VichImageType::class, [
            'label' => 'Joindre un document',
            'required'     => false,
            'allow_delete' => false,
            'download_uri' => false
            ])
                
            ->add('captcha', CaptchaType::class)
            ->add('Envoyer', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Contribution::class,
        ]);
    }
}
