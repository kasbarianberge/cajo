<?php

namespace App\Entity;

use App\Repository\KeywordsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=KeywordsRepository::class)
 */
class Keywords
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank
     * @ORM\Column(type="string", length=255)
     */
    private string $keyword;

    /**
     * @ORM\Column(type="datetime")
     */
    private ?\DateTime $createdAt;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\News", mappedBy="listekeywords")
     */
    private $mentionsNews;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Tuto", mappedBy="listekeywords")
     */
    private $mentionsTutos;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Jurisprudence", mappedBy="listekeywords")
     */
    private $mentionsJp;




    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->mentionsNews = new ArrayCollection();
        $this->mentionsTutos = new ArrayCollection();
        $this->mentionsJp = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getKeyword(): ?string
    {
        return $this->keyword;
    }

    public function setKeyword(string $keyword): self
    {
        $this->keyword = $keyword;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    //MANY TO MANY NEWS -> KEYWORDS
    public function getMentionsNews(): Collection
    {
        return $this->mentionsNews;
    }

    public function addMentionNews(News $news): self
    {
        if (!$this->mentionsNews->contains($news)) {
            $this->mentionsNews[] = $news;
            $news->addListekeywords($this);
        }

        return $this;
    }

    public function removeMentionNews(News $news): self
    {
        if ($this->mentionsNews->removeElement($news)) {
            $news->removeListekeywords($this);
        }

        return $this;
    }


    //MANY TO MANY TUTOS -> KEYWORDS
    public function getMentionsTutos(): Collection
    {
        return $this->mentionsTutos;
    }

    public function addMentionTuto(Tuto $tuto): self
    {
        if (!$this->mentionsTutos->contains($tuto)) {
            $this->mentionsTutos[] = $tuto;
            $tuto->addListekeywords($this);
        }

        return $this;
    }

    public function removeMentionTuto(Tuto $tuto): self
    {
        if ($this->mentionsTutos->removeElement($tuto)) {
            $tuto->removeListekeywords($this);
        }

        return $this;
    }



    //MANY TO MANY JPs -> KEYWORDS
    public function getMentionsJp(): Collection
    {
        return $this->mentionsJp;
    }

    public function addMentionJp(Jurisprudence $jurisprudence): self
    {
        if (!$this->mentionsJp->contains($jurisprudence)) {
            $this->mentionsJp[] = $jurisprudence;
            $jurisprudence->addListekeywords($this);
        }

        return $this;
    }

    public function removeMentionJp(Jurisprudence $jurisprudence): self
    {
        if ($this->mentionsJp->removeElement($jurisprudence)) {
            $jurisprudence->removeListekeywords($this);
        }

        return $this;
    }

}
