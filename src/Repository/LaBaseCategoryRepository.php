<?php

namespace App\Repository;

use App\Entity\LaBaseCategory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method LaBaseCategory|null find($id, $lockMode = null, $lockVersion = null)
 * @method LaBaseCategory|null findOneBy(array $criteria, array $orderBy = null)
 * @method LaBaseCategory[]    findAll()
 * @method LaBaseCategory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LaBaseCategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LaBaseCategory::class);
    }

    // /**
    //  * @return LaBaseCategory[] Returns an array of LaBaseCategory objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }

    
    public function findOneBySomeField($value): ?LaBaseCategory
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }*/
}

