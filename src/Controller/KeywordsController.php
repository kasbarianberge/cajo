<?php

namespace App\Controller;

use App\Entity\Keywords;
use App\Repository\KeywordsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class KeywordsController extends AbstractController
{
    private KeywordsRepository $keywordsRepository;

    public function __construct(KeywordsRepository $keywordsRepository) {$this->keywordsRepository = $keywordsRepository;}

    /**
     * @Route("/keyword", name="keyword")
     */
    public function keywordsRead(Request $request)
    {
        $data = [];
        $category = $request->query->get('categorie');
        $keywordId = $request->query->get('keyword');
    
        $keyword = $this->keywordsRepository->find((int) $keywordId);
        if (null === $keyword) {
            throw new NotFoundHttpException();
        }

        if (null === $category || 'infos' === $category) {
            // Get news
            $news = $keyword->getMentionsNews();
            /** @var News $new */
            foreach ($news as $new) {
                if ($new->isActive() === true) {
                    $newsData = [
                        'title' => $new->getTitle(),
                        'content' => $this->cleanContent($new->getContent()),
                        'type' => 'news',
                        'id' => $new->getId()
                    ];
                    $data[$new->getCreatedAt()->getTimestamp()] = $newsData;
                }
            }
        }

        if (null === $category || 'tutos' === $category) {
            // Get tutos
            $tutos = $keyword->getMentionsTutos();
            /** @var Tuto $tuto */
            foreach ($tutos as $tuto) {
                if ($tuto->isActive() === true) {
                    $tutoData = [
                        'title' => $tuto->getTitle(),
                        'content' => $this->cleanContent($tuto->getContent()),
                        'type' => 'tuto',
                        'id' => $tuto->getId()
                    ];
                    $data[$tuto->getCreatedAt()->getTimestamp()] = $tutoData;
                }
            }
        }

        if (null === $category || 'jurisprudences' === $category) {
            // Get jp
            $jurisprudences = $keyword->getMentionsJp();
            /** @var Jurisprudence $jurisprudence */
            foreach ($jurisprudences as $jurisprudence) {
                if ($jurisprudence->isActive() === true) {
                    $jpData = [
                        'title' => $jurisprudence->getName(),
                        'content' => $this->cleanContent($jurisprudence->getContent()),
                        'type' => 'jurisprudence',
                        'id' => $jurisprudence->getId()
                    ];
                    $data[$jurisprudence->getCreatedAt()->getTimestamp()] = $jpData;
                }
            }
        }
        
        ksort($data);

        return $this->render('search/keyword.html.twig', [
            'controller_name' => 'HomepageController',
            'active_menu' => 'homepage',
            'dataContent' => $data,
            'categoryRequest' => $category,
            'keyword' => $keyword
        ]);
}


private function cleanContent(string $content)
{
    $contentClean = strip_tags($content, '<br/><br>');
    $contentClean = str_replace("&rsquo;", "'", $contentClean);
    $contentClean = str_replace("&#39;", "'", $contentClean);

    return $contentClean;
}

}