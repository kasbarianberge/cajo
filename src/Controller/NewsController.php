<?php

namespace App\Controller;

use App\Entity\News;
use App\Repository\NewsRepository;
use App\Repository\NewsCategoryRepository;
use App\Services\VisitorCountManager;
use Knp\Component\Pager\Paginator;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\VisitorCount;
use App\Repository\TutoRepository;
use App\Repository\VisitorCountRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class NewsController extends AbstractController
{
    private NewsRepository $newsRepository;
    private NewsCategoryRepository $newsCategoryRepository;
    private VisitorCountManager $visitorCountManager;

    public function __construct(
        NewsRepository $newsRepository,
        NewsCategoryRepository $newsCategoryRepository,
        VisitorCountManager $visitorCountManager
    ) {
        $this->newsRepository = $newsRepository;
        $this->newsCategoryRepository = $newsCategoryRepository;
        $this->visitorCountManager = $visitorCountManager;
    }

    /**
     * @Route("/actus", name="news_list")
     */
    public function newsList(Request $request, PaginatorInterface $paginator): Response
    {
        $categories = $this->newsCategoryRepository->findBy([
            'enabled' => true
        ], [
            'name' => 'asc'
        ]);

        // Stats total
        /*$totalCount = $this->visitorCountRepository->findOneBy(['type' => VisitorCount::TOTAL_VISITORS_TYPE]);
        if ($totalCount) {
            $totalCount->setCounter($totalCount->getCounter() + 1);
        } else {
            $totalCount = new VisitorCount();
            $totalCount->setType(VisitorCount::TOTAL_VISITORS_TYPE);
            $totalCount->setCounter(1);
            $this->entityManager->persist($totalCount);
        }
        $this->entityManager->flush();*/

        $categoryId = $request->query->get('category');
        if (null !== $categoryId) {
            $category = $this->newsCategoryRepository->find((int) $categoryId);
            $query = $this->newsRepository->findNewsActiveQueryByCategory($category);
        } else {
            $category = null;
            $query = $this->newsRepository->findNewsActiveQueryByCategory();
        }

        $data = $paginator->paginate($query, $request->query->get('page', 1), 6);

        $items = $data->getItems();
        $itemsWithoutTags = [];
        foreach ($items as $item) {
            $content = $item->getContent();
            $contentClean = strip_tags($content, '<br/><br>');
            $contentClean = str_replace("&rsquo;", "'", $contentClean);
            $contentClean = str_replace("&#39;", "'", $contentClean);
            $contentClean = str_replace("<br><br>", "<br>", $contentClean);
            $item->setContent($contentClean);
            $itemsWithoutTags[] = $item;
        }

        $data->setItems($itemsWithoutTags);
        
        return $this->render('news/news_list.html.twig', [
            'controller_name' => 'NewsController',
            'active_menu' => 'news',
            'categories' => $categories,
            'categoryRequest' => $category,
            'news' => $data
        ]);
    }

    /**
     * @Route("/lire-actu/{actuId}", name="news_read")
     */
    public function newsRead(string $actuId)
    {
        $news = $this->newsRepository->find((int) $actuId);
        if (null === $news) {
            throw new NotFoundHttpException();
        }
        
        // Count visitors
        $this->visitorCountManager->incrementStats($news);

        return $this->render('news/news_read.html.twig', [
            'controller_name' => 'NewsController',
            'active_menu' => 'news',
            'news' => $news
        ]);
    }
}
