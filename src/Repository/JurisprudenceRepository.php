<?php

namespace App\Repository;

use App\Entity\Jurisprudence;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Jurisprudence|null find($id, $lockMode = null, $lockVersion = null)
 * @method Jurisprudence|null findOneBy(array $criteria, array $orderBy = null)
 * @method Jurisprudence[]    findAll()
 * @method Jurisprudence[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JurisprudenceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Jurisprudence::class);
    }

    public function findJPByTitle(?string $title = null)
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.active = :active')
            ->andWhere('j.name LIKE :name')
            ->setParameter('active', true)
            ->setParameter('name', '%'.$title.'%')
            ->orderBy('j.createdAt', 'DESC')
            ->getQuery()
            ->getResult()
            ;
    }

    /* askip inutile public function findJPBycontent(?string $content = null)
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.active = :active')
            ->andWhere('j.content LIKE :content')
            ->setParameter('active', true)
            ->setParameter('content', '%'.$content.'%')
            ->orderBy('j.createdAt', 'DESC')
            ->getQuery()
            ->getResult()
            ;
    }*/

    public function findJPActiveQuery()
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.active = :active')
            ->setParameter('active', true)
            ->orderBy('j.createdAt', 'DESC')
            ->getQuery()
            ;
    }

    //rechercher par numéro de rg + jp active
    public function findJPByRg(?string $rg = null)
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.active = :active')
            ->andWhere('j.rg LIKE :rg')
            ->setParameter('active', true)
            ->setParameter('rg', '%'.$rg.'%')
            ->orderBy('j.createdAt', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function FindJPByCategory($categories)
    {
        return $this->createQueryBuilder('j')
            ->join('j.category', 'c')
            ->andWhere('j.active = :active')
            ->andWhere('c.id IN (:categories)')
            ->setParameter('active', true)
            ->setParameter('categories', $categories)
            ->orderBy('j.important', 'DESC')
            ->AddOrderBy('j.createdAt', 'DESC')
            ->getQuery()
            ->getResult();
    }
}
