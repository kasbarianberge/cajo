<?php

namespace App\Entity;

use App\Repository\LexiqueRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=LexiqueRepository::class)
 */
class Lexique
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $active = false;

    /**
     * @Assert\NotBlank
     * @ORM\Column(type="string", length=255)
     */
    private string $word;

    /**
     * @Assert\NotBlank
     * @ORM\Column(type="text")
     */
    private string $definition;

    /**
     * @ORM\Column(type="datetime")
     */
    private ?\DateTime $createdAt;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\News", mappedBy="listelexique")
     */
    private $mentionsNews;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Tuto", mappedBy="listelexique")
     */
    private $mentionsTutos;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Jurisprudence", mappedBy="listelexique")
     */
    private $mentionsJp;









    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->mentionsNews = new ArrayCollection();
        $this->mentionsTutos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getWord(): ?string
    {
        return $this->word;
    }

    public function setWord(string $word): self
    {
        $this->word = $word;

        return $this;
    }

    public function getDefinition(): ?string
    {
        return $this->definition;
    }

    public function setDefinition(string $definition): self
    {
        $this->definition = $definition;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): void
    {
        $this->active = $active;
    }


    //MANY TO MANY NEWS -> LEXIQUE
    public function getMentionsNews(): Collection
    {
        return $this->mentionsNews;
    }

    public function addMentionNews(News $news): self
    {
        if (!$this->mentionsNews->contains($news)) {
            $this->mentionsNews[] = $news;
            $news->addListelexique($this);
        }

        return $this;
    }

    public function removeMentionNews(News $news): self
    {
        if ($this->mentionsNews->removeElement($news)) {
            $news->removeListelexique($this);
        }

        return $this;
    }


    //MANY TO MANY TUTOS -> LEXIQUE
    public function getMentionsTutos(): Collection
    {
        return $this->mentionsTutos;
    }

    public function addMentionTuto(Tuto $tuto): self
    {
        if (!$this->mentionsTutos->contains($tuto)) {
            $this->mentionsTutos[] = $tuto;
            $tuto->addListelexique($this);
        }

        return $this;
    }

    public function removeMentionTuto(Tuto $tuto): self
    {
        if ($this->mentionsTutos->removeElement($tuto)) {
            $tuto->removeListelexique($this);
        }

        return $this;
    }



    //MANY TO MANY JPs -> LEXIQUE
    public function getMentionsJp(): Collection
    {
        return $this->mentionsJp;
    }

    public function addMentionJp(Jurisprudence $jurisprudence): self
    {
        if (!$this->mentionsJp->contains($jurisprudence)) {
            $this->mentionsJp[] = $jurisprudence;
            $jurisprudence->addListelexique($this);
        }

        return $this;
    }

    public function removeMentionJp(Jurisprudence $jurisprudence): self
    {
        if ($this->mentionsJp->removeElement($jurisprudence)) {
            $jurisprudence->removeListelexique($this);
        }

        return $this;
    }
}
