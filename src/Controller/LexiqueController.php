<?php

namespace App\Controller;

use App\Entity\Lexique;
use App\Repository\LexiqueRepository;
use App\Services\VisitorCountManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class LexiqueController extends AbstractController
{
    private LexiqueRepository $lexiqueRepository;

    public function __construct(LexiqueRepository $lexiqueRepository) {$this->lexiqueRepository = $lexiqueRepository;}

    /**
     * @Route("/lexique", name="lexique")
     */
    public function index(Request $request): Response
    {
        $entries = $this->lexiqueRepository->findActiveDefsQuery();

        return $this->render('lexique/lexique-full.html.twig', [
            'controller_name' => 'LexiqueController',
            'active_menu' => 'base',
            'entries' => $entries
        ]);
    }

}
