<?php

namespace App\Repository;

use App\Entity\Lexique;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Lexique|null find($id, $lockMode = null, $lockVersion = null)
 * @method Lexique|null findOneBy(array $criteria, array $orderBy = null)
 * @method Lexique[]    findAll()
 * @method Lexique[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LexiqueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Lexique::class);
    }

    //liste des définitions actives
    public function findActiveDefsQuery()
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.active = :active')
            ->setParameter('active', true)
            ->orderBy('d.word', 'ASC')
            ->getQuery()
            ->getArrayResult()
        ;
    }
}
